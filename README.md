# FTB Server Container

This repository contains Dockerfile for running a [FeedTheBeast](http://feed-the-beast.com/) Server.

# Base Docker Image

* [dockerfile/java](https://registry.hub.docker.com/u/dockerfile/java/)

# Docker Tags

`deltatrees/ftbserver` provides multiple tagged images:

* `latest` (default) alias to `1.0.3`
* `1.0.3` Version 1.0.3 of FTB Direwolf20 Modpack

## Installation

1. Install [Docker](https://www.docker.com/).
2. Download [automated build](https://registry.hub.docker.com/u/deltatrees/ftbserver/) from public [Docker Hub Registry](https://registry.hub.docker.com/): `docker pull deltatrees/ftbserver`

## Usage
```
docker run --name="ftbserver" -p 25565:25565 -v /data/ftbserver/world:/ftb/world -v  /data/ftbserver/world:/ftb/logs -d deltatrees/ftbserver
```

## Exposed port
* 25565

## ENV variables and defaults
* MODPACK_NAME "direwolf20_17"
* MODPACK_VERSION "1_0_3"
* MODPACK_FILE "direwolf20_17-server.zip"

## Volume
Mountable volumes are:

* /ftb/world
* /ftb/logs